set iminsert=0
syntax on
set number
set cursorline
set tags=.git/tags,~/packages/mpulib3/.git/tags,~/packages/uniset2/.git/tags,~/packages/tags
set autoread
set smartindent
set path+=/usr/include/gtkmm-3.0
set clipboard=unnamedplus 
set backspace=indent,eol,start
colorscheme jellybeans
let g:ycm_global_ycm_extra_conf = '~/.vim/plugged/YouCompleteMe/.ycm_extra_conf.py'
let g:ycm_max_diagnostics_to_display = 0
let g:ycm_show_diagnostics_ui = 0
let g:molokai_original = 1
let g:jellybeans_use_lowcolor_black = 1
let g:autotagTagsFile=".tags"
let g:cmake_ycm_symlinks = 1
let g:cmake_export_compile_commands = 1
let g:airline_powerline_fonts = 1
let g:airline#extensions#keymap#enabled = 0
let g:airline_section_z = "\ue0a1:%l/%L Col:%c"
let g:Powerline_symbols='unicode'
let g:airline#extensions#xkblayout#enabled = 0

set guioptions+=c

map <F3> :NERDTreeToggle<CR>
map <F5> :Autoformat<CR>
map <F6> :TagsGenerate!<CR>
map <F2> :w<CR>
map <F10> :wq<CR>
map <F4> :q!<CR>
call plug#begin()
Plug 'vim-scripts/XML-Completion'
Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'ajh17/spacegray.vim'
Plug 'scrooloose/nerdtree'
Plug 'chiel92/vim-autoformat'
Plug 'tomasr/molokai'
Plug 'nanotech/jellybeans.vim'
Plug 'vim-scripts/vim-gtest'
Plug 'craigemery/vim-autotag'
Plug 'szw/vim-tags'
Plug 'vitalk/vim-simple-todo'
Plug 'vhdirk/vim-cmake'
Plug 'vim-syntastic/syntastic'
Plug 'jbakamovic/yavide'
Plug 'ycm-core/YouCompleteMe'
Plug 'vim-airline/vim-airline'
Plug 'thosakwe/vim-flutter'
Plug 'dart-lang/dart-vim-plugin'
Plug 'neoclide/coc.nvim'
call plug#end()
